<?php

namespace App\Http\Controllers;

use App\Services\ScraperService;
use Goutte\Client;
use Illuminate\Http\Request;
use spekulatius\phpscraper;

class ScraperController extends Controller
{

    /**
     * @var ScraperService
     */
    private ScraperService $scraperService;

    public function __construct(ScraperService $scraperService)
    {

        $this->scraperService = $scraperService;
    }
    public function index()
    {
//        $web = new phpscraper();
//
//        $web->go('https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22#_5576');
//
//
//        $pattern = '/https:\/\/search.ipaustralia.gov.au\/trademarks\/search\/result\?s+?/m';
//        $redirectLinks = [];
//        foreach ($web->links as $link) {
//            if(preg_match($pattern, $link)){
//                if (!in_array($link, $redirectLinks)){
//                    $redirectLinks[] = $link;
//                }
//            }
//
//        }
//
//        $web->go('https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=8');
//
//        foreach ($web->links as $link) {
//            if(preg_match($pattern, $link)){
//                if (!in_array($link, $redirectLinks)){
//                    $redirectLinks[] = $link;
//                }
//            }
//
//        }
//
//        var_dump($redirectLinks);

//        $web->go('https://search.ipaustralia.gov.au/trademarks/search/view/124100');
//        foreach ($web->links as $link) {
//           echo $link;
//
//
//        }
//        echo count($web->links);
//        $client = new Client();
//
//        $redirectLinks = [];
//        $crawler = $client->request('GET', 'https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=5');
//        //goto-page
//        $gotoPage =  $crawler->filter('.goto-page')->each(function ($node, $i) {
//            return $node->extract(['href']);
//
//        });
//
//        $redirectLinks = [];
//        foreach ($gotoPage as $link) {
//            if (!in_array($link[0], $redirectLinks)){
//                $redirectLinks[] = $link[0];
//            }
//
//        }
//
//        $crawler = $client->request('GET', 'https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22#_5576');
//        //goto-page
//        $gotoPage =  $crawler->filter('.goto-page')->each(function ($node, $i) {
//            return $node->extract(['href']);
//
//        });
//
//        foreach ($gotoPage as $link) {
//            if (!in_array($link[0], $redirectLinks)){
//                $redirectLinks[] = $link[0];
//            }
//
//        }

//        var_dump($redirectLinks);

        //number qa-tm-number
        $client = new Client();
        $crawler = $client->request('GET', 'https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=0');

        $urls =  $crawler->filter('.qa-tm-number')->each(function ($node, $i) {
            return $node->extract(['href']);

        });
        $dataUrl = [];
        foreach ($urls as $link) {
            if ( ! in_array( $link[0], $dataUrl ) ) {
                $dataUrl[] = $link[0];
            }
        }
        var_dump($dataUrl);

    }

    public function getData()
    {
        $client = new Client();

        $crawler = $client->request('GET', 'https://search.ipaustralia.gov.au/trademarks/search/view/5576?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=0');

        $keys =  $crawler->filter('.list-of-definition dt')->each(function ($node, $i) {
            return $node->text();

        });

        $crawler = $client->request('GET', 'https://search.ipaustralia.gov.au/trademarks/search/view/5576?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=0');
        $values = $crawler->filter('.list-of-definition dd')->each(function ($node, $i) {
             return $node->text();

        });
        $res = [];
        $result = array_map(function ($key, $value){
            $array = [];
            if ($key == 'Number'){
                 $array[$key] = $value;
            }elseif ($key == 'Words'){
                 $array[$key] = $value;
            }elseif($key == 'Class'){
                 $array[$key] = $value;
            }elseif ($key == 'Status'){
                 $array[$key] = $value;
            }
            return $array;
        }, $keys, $values);

        foreach ($result as $key => $item){
            foreach ($item as $i => $value){
                $res[$i] = $value;
            }

        }
//        $result = array_combine($keys, $values);
        var_dump($res);
//        $output = [
//            'number' => $result['Number'],
//            'name' => $result['Words'],
//            'classes' => $result['Class'],
//            'status' => $result['Status'],
//            'details_page_url' => 'https://search.ipaustralia.gov.au/trademarks/search/view/48465?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=0'
//        ];
//        var_dump($keys, $values);

    }

    public function getAllDataFromLinksGenerator()
    {
        set_time_limit(1400);
        $goToLinks = $this->scraperService->getAllGoToPageUrl();
        $dataUrls = [];
        foreach ( $goToLinks as $key => $item) {
            $dataUrls[] = $this->scraperService->getDataUrls('https://search.ipaustralia.gov.au' . $item);
        }

        $data = [];
        foreach ( $dataUrls as $key => $item) {
            foreach ( $item as $value ) {
                yield json_encode($this->scraperService->getDataFromUrl('https://search.ipaustralia.gov.au' . $value));
            }
        }
        var_dump($data);
    }

    public function getAllDataFromLinks()
    {
        foreach ($this->getAllDataFromLinksGenerator() as $item){
            echo $item;
        }

    }
}
