<?php


namespace App\Services;


use Goutte\Client;

class ScraperService
{
    public function getDataFromUrl($url)
    {
        $client = new Client();

        $crawler = $client->request('GET', $url);

        $keys =  $crawler->filter('.list-of-definition dt')->each(function ($node, $i) {
            return $node->text();

        });

        $crawler = $client->request('GET', $url);
        $values = $crawler->filter('.list-of-definition dd')->each(function ($node, $i) {
            return $node->text();

        });

        $res = array_map(function ($key, $value){
            $array = [];
            if ($key == 'Number'){
                 $array[$key] = $value;
            }elseif ($key == 'Words'){
                 $array[$key] = $value;
            }elseif($key == 'Class'){
                 $array[$key] = $value;
            }elseif ($key == 'Status'){
                 $array[$key] = $value;
            }
            return $array;
        }, $keys, $values);
        $result = [];
        foreach ($res as $key => $item){
            foreach ($item as $i => $value){
                $result[$i] = $value;
            }

        }
//        $result = array_combine($keys, $values);
        return [
            'number' => $result['Number'] ?? '',
            'name' => $result['Words'] ?? '',
            'classes' => $result['Class'] ?? '',
            'status' => $result['Status'] ?? '',
            'details_page_url' => $url
        ];

    }

    public function getDataUrls($url)
    {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $urls =  $crawler->filter('.qa-tm-number')->each(function ($node, $i) {
            return $node->extract(['href']);

        });
        $dataUrl = [];
        foreach ($urls as $link) {
            if ( ! in_array( $link[0], $dataUrl ) ) {
                $dataUrl[] = $link[0];
            }
        }

        return $dataUrl;
    }

    public function getAllGoToPageUrl()
    {

        $redirectLinks = $this->getToPageUrls('https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22&p=5');
        return $this->getToPageUrls('https://search.ipaustralia.gov.au/trademarks/search/result?s=030cf6a5-2281-43d2-bcc3-7dfbe1dd3a22#_5576', $redirectLinks);
    }

    private function getToPageUrls($url, $redirectLinks = [])
    {
        $client = new Client();

        $crawler = $client->request('GET', $url);
        //goto-page
        $gotoPage =  $crawler->filter('.goto-page')->each(function ($node, $i) {
            return $node->extract(['href']);

        });

        foreach ($gotoPage as $link) {
            if (!in_array($link[0], $redirectLinks)){
                $redirectLinks[] = $link[0];
            }

        }

        return $redirectLinks;
    }

    public function getAllDataFromLinksGenerator()
    {
        set_time_limit(1400);
        $goToLinks = $this->getAllGoToPageUrl();
        $dataUrls = [];
        foreach ( $goToLinks as $key => $item) {
            $dataUrls[] = $this->getDataUrls('https://search.ipaustralia.gov.au' . $item);
        }

        foreach ( $dataUrls as $key => $item) {
            foreach ( $item as $value ) {
                yield json_encode($this->getDataFromUrl('https://search.ipaustralia.gov.au' . $value));
            }
        }
    }

    public function getAllDataFromLinks()
    {
        foreach ($this->getAllDataFromLinksGenerator() as $item){
            echo $item;
        }

    }
}
